import { getCsrf } from '@/api/public'
import Cookies from 'js-cookie'

getCsrf().then(res => {
  Cookies.set('csrftoken', res.data.data.token)
})
