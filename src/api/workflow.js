import axios from 'axios'

export const getTicket = async ticketId => {
  return axios.get(`/workflow/ticket/details/${ticketId}/`)
}
