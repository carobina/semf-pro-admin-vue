import axios from 'axios'
import dayjs from 'dayjs'
function formateFormData(data) {
  data.manage = data.manage.toString()
  data.asset = data.asset.toString()
  data.start_time = dayjs(data.start_time).format('YYYY-MM-DD HH:mm:ss')
  return data
}

export async function createTask(data) {
  const formData = formateFormData(data)
  return axios.post('/task/task/create/', formData)
}

export async function taksType() {
  return axios.get('/task/type/select/')
}

export async function deleteTask(taskId) {
  return axios.get(`/task/task/delete/${taskId}/`)
}

export async function updateTask(data) {
  return axios.post(`/task/task/update/${data.id}/`, data)
}

export async function taskVuln(taskId) {
  return axios.get(`/task/vuln/list/<task_id>/${taskId}/`)
}
