import axios from 'axios'

export const echoLog = async(key) => {
  return axios.get(`/list/echolist/${key}/`)
}
