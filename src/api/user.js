import axios from 'axios'

export const createUser = async data => {
  return axios.post('/usermanage/user/create/', data)
}

export const ChangeUserStatus = async userId => {
  return axios.get(`/usermanage/user/deny/${userId}/`)
}

export const getRoles = async() => {
  return axios.get('/rbac/list/roles/')
}
