import axios from 'axios'

export async function getCsrf() {
  return axios.get('/get_token/')
}

export const getCaptcha = async() => {
  return axios.get('/captcha/refresh/', {
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    }
  })
}
