import axios from 'axios'

export async function vulnType() {
  return axios.get(`/vuln/type/select/`)
}

export async function vulnLevel() {
  return axios.get(`/vuln/level/select/`)
}

export async function vulnAsset(key) {
  return axios.get(`/asset/asset/select/`, { params: { key }})
}

export async function createVuln(data) {
  return axios.post('/vuln/vuln/create/', data)
}
