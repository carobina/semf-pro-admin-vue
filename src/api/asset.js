import axios from 'axios'

export async function getTypeList() {
  return axios.get('/asset/type/select/')
}

export async function getAssetDetial(assetId) {
  return axios.get(`/asset/asset/details/${assetId}/`)
}

export async function delAsset(assetId) {
  return axios.get(`/asset/asset/delete/${assetId}/`)
}

export async function getAssetList(key) {
  return axios.get('/asset/asset/list/', {
    params: { page: 1, limit: 5, key }
  })
}

export async function getGroupList(key) {
  return axios.get('/asset/group/list/', {
    params: { page: 1, limit: 5, key }
  })
}

export async function createAsset(data) {
  return axios.post('/asset/asset/create/', data)
}

export async function updateAsset(assetId, data) {
  return axios.post(`/asset/asset/update/${assetId}/`, data)
}

export async function getWebLanguage() {
  return axios.get('/asset/language/select/')
}

export async function assetSelect(key) {
  return axios.get(`/asset/asset/select/`, { params: { key }})
}
