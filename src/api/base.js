import axios from 'axios'

export const getUserList = async key => {
  return axios.get('/base/select/person/', {
    params: { page: 1, limit: 5, key }
  })
}

export const creatPerson = async data => {
  return axios.post('/base/create/person/', data)
}

export const updatePerson = async data => {
  return axios.post(`base/update/person/${data.id}/`, data)
}

export const delPerson = async personId => {
  return axios.get(`/base/delete/person/${personId}/`)
}

export const getDepList = async(key = null) => {
  return axios.get('/base/list/department/', {
    params: { page: 1, limit: 5, key }
  })
}

export const creatClient = async data => {
  return axios.post('/base/create/client/', data)
}

export const updateClient = async data => {
  return axios.post(`base/update/client/${data.id}/`, data)
}

export const delClient = async clientId => {
  return axios.get(`/base/delete/client/${clientId}/`)
}

export const createVlan = async data => {
  return axios.post('/base/create/vlancreate/', data)
}

export const updateVlan = async data => {
  return axios.post(`base/update/vlan/${data.id}/`, data)
}

export const delVlan = async vlanId => {
  return axios.get(`/base/delete/vlan/${vlanId}/`)
}

export const createDomain = async data => {
  return axios.post('/base/create/domaincreate/', data)
}

export const updateDomain = async data => {
  return axios.post(`base/update/domain/${data.id}/`, data)
}

export const delDomain = async domainId => {
  return axios.get(`/base/delete/domain/${domainId}/`)
}

export const createDepartment = async data => {
  return axios.post('/base/create/department/', data)
}

export const updateDepartment = async data => {
  return axios.post(`base/update/department/${data.id}/`, data)
}

export const delDepartment = async domainId => {
  return axios.get(`/base/delete/department/${domainId}/`)
}

export const createPermission = async data => {
  return axios.post('/base/create/department/', data)
}

export const updatePermission = async data => {
  return axios.post(`base/update/department/${data.id}/`, data)
}

export const delPermission = async permissionId => {
  return axios.get(`/base/delete/department/${permissionId}/`)
}

export const createArea = async data => {
  return axios.post('/base/create/areacreate/', data)
}

export const updateArea = async data => {
  return axios.post(`base/update/area/${data.id}/`, data)
}

export const delArea = async areaId => {
  return axios.get(`/base/delete/area/${areaId}/`)
}
