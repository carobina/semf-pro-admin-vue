/**
 * 路由配置
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import setting from '@/config/setting'
import EleLayout from '@/views/common/layout/Layout'
import NProgress from 'nprogress'

Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

/* 静态路由配置 */
const routes = [
  {
    path: '/login',
    component: () => import('@/views/login/login'),
    meta: { hide: true, title: '登录' }
  },
  {
    path: '/admin',
    component: () => import('@/views/admin/login'),
    meta: { hide: true, title: '管理后台' }
  },
  {
    path: '/forget',
    component: () => import('@/views/login/forget'),
    meta: { hide: true, title: '忘记密码' }
  },
  {
    path: '/share',
    component: () => import('@/views/vuln/share'),
    meta: { hide: true, title: '漏洞分享' }
  },
  // 嵌套路由需要单独配置, 菜单不完全等于路由
  {
    path: '/asset',
    component: EleLayout,
    meta: { title: '资产管理' },
    children: [
      {
        path: 'rootdetial/:id',
        component: () => import('@/views/asset/list/index'),
        meta: { title: '业务线详情' }
      }
    ]
  },
  {
    path: '/vuln',
    component: EleLayout,
    meta: { title: '漏洞管理' },
    children: [
      {
        path: 'detial/:id',
        component: () => import('@/views/vuln/detial'),
        meta: { title: '漏洞详情' }
      },
      {
        path: 'edit/:id',
        component: () => import('@/views/vuln/list/edit'),
        meta: { title: '编辑漏洞' }
      },
      {
        path: 'create',
        component: () => import('@/views/vuln/list/edit'),
        meta: { title: '创建漏洞' }
      }
    ]
  },
  {
    path: '/task',
    component: EleLayout,
    meta: { title: '任务管理' },
    children: [
      {
        path: 'detial/:id',
        component: () => import('@/views/task/detial'),
        meta: { title: '任务详情' }
      }
    ]
  },
  {
    path: '/system',
    component: EleLayout,
    meta: { title: '系统管理' },
    linkActiveClass: 'is-active',
    children: [
      {
        path: 'base',
        redirect: '/system/base/department',
        component: () => import('@/views/system/base'),
        meta: { title: '基础信息' },
        children: [
          {
            path: 'department',
            component: () => import('@/views/system/base/department')
          }, {
            path: 'person',
            component: () => import('@/views/system/base/person')
          }, {
            path: 'client',
            component: () => import('@/views/system/base/client')
          }, {
            path: 'vlan',
            component: () => import('@/views/system/base/vlan')
          }, {
            path: 'domain',
            component: () => import('@/views/system/base/domain')
          }, {
            path: 'area',
            component: () => import('@/views/system/base/area')
          },
          {
            path: 'scanner',
            component: () => import('@/views/system/base/scanner')
          }
        ]
      }
    ]
  }
]

// 404路由在动态路由后面加
const route404 = {
  path: '',
  component: EleLayout,
  meta: { hide: true },
  children: [
    {
      path: '*',
      component: () => import('@/views/common/exception/404'),
      meta: { hide: true, title: '404' }
    }
  ]
}

const router = new VueRouter({
  routes
})

/* 路由守卫 */
router.beforeEach((to, from, next) => {
  NProgress.start()
  document.title = `${to.meta.title || ''}${to.meta.title ? ' - ' : ''}${setting.name}`
  if (store.state.user.token) { // 判断是否登录
    if (!store.state.user.menus) { // 判断是否已注册动态路由
      store.dispatch('user/getMenuRouters').then(route => { // 获取动态路由
        if (route && route.children) {
          route.component = EleLayout
          // 去除已注册的路由
          for (let i = route.children.length - 1; i >= 0; i--) {
            if (router.resolve(route.children[i].path).resolved.matched.length) {
              route.children.splice(i, 1)
            }
          }
          router.addRoutes([route, route404])
        }
        next({ ...to, replace: true })
      }).catch(() => {
      })
    } else {
      next()
    }
  } else if (setting.whiteList.indexOf(to.path) !== -1) { // 判断是否在无需登录白名单
    next()
  } else { // 未登录跳转登录页面
    next({ path: '/login', query: to.path === '/' ? {} : { from: to.path }})
  }
})

router.afterEach(() => {
  setTimeout(() => {
    NProgress.done()
  }, 150)
})

export default router
