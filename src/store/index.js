/**
 * Vuex状态管理
 */
import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import theme from './modules/theme'
import user from './modules/user'
import asset from './modules/asset'
import vuln from './modules/vuln'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    queryKey: ''
  },
  mutations: {},
  actions: {},
  modules: {
    theme,
    user,
    asset,
    vuln
  },
  getters
})
