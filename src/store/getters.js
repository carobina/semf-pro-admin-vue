export default {
  theme: state => state.theme,
  user: state => state.user,
  asset: state => state.asset,
  vuln: state => state.vuln
}
