import { getAssetDetial } from '@/api/asset'

export default {
  namespaced: true,
  state: {
    type: '',
    assetInfo: {
      id: 0,
      key: '',
      name: '',
      type: {
        id: 0,
        name: ''
      },
      manage: {
        id: 0,
        name: ''
      },
      is_out: '',
      is_use: '',
      add_time: '',
      update_time: '',
      description: ''
    },
    typeInfo: {},
    hasPlugin: false,
    hasPort: false
  },
  mutations: {
    /* 设置资产信息 */
    setAssetInfo(state, value) {
      state.assetInfo = value
    },
    /* 设置资产类型信息 */
    setTypeInfo(state, value) {
      state.typeInfo = value
    },
    /* 设置资产类型 */
    setAssetType(state, value) {
      state.type = value
    },
    /* 设置组件列表状态 */
    setPluginStatus(state, value) {
      state.hasPlugin = value
    },
    /* 设置端口列表状态 */
    setPortStatus(state, value) {
      state.hasPort = value
    }
  },
  actions: {
    /* 获得资产信息 */
    async getAssetInfo({ commit }, assetId) {
      commit('setPluginStatus', false)
      commit('setPortStatus', false)
      const resp = await getAssetDetial(assetId)
      if (resp.data.code === 0) {
        const { data } = resp.data
        commit('setAssetInfo', data.info)
        Object.keys(data).forEach(item => {
          if (item === 'web' || item === 'os') {
            commit('setAssetType', item)
            commit('setTypeInfo', data[item])
          }
          if (item === 'plugin') {
            commit('setPluginStatus', true)
          } else if (item === 'port') {
            commit('setPortStatus', true)
          }
        })
      }
    }
  }
}
