export default {
  namespaced: true,
  state: {
    name: '',
    type: {
      id: 1,
      name: ''
    },
    level: {
      id: 1,
      name: ''
    },
    asset: [],
    fix: '',
    intorduce: ''
  },
  mutations: {
    updateValue(state, payload) {
      state.value = payload
    }
  },
  actions: {
    updateValue({ commit }, payload) {
      commit('updateValue', payload)
    }
  }
}
