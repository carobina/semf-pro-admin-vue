const taskFormRules = {
  name: [
    { required: true, message: '请输入任务名称', trigger: 'blur' }
  ],
  target: [
    { required: true, message: '请输入任务目标', trigger: 'blur' }
  ],
  type: [
    { required: true, message: '请选择资产类型', trigger: 'blur' }
  ],
  person: [
    { required: true, message: '请选择对接人', trigger: 'blur' }
  ],
  manage: [
    { required: true, message: '请选择负责人', trigger: 'blur' }
  ],
  time: [
    { required: true, message: '请选择任务开始时间', trigger: 'change' }
  ]
}
export default taskFormRules
