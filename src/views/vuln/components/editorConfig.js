const editorConfig = {
  menu: [
    'head',
    'bold',
    'fontSize',
    'fontName',
    'italic',
    'underline',
    'strikeThrough',
    'indent',
    'lineHeight',
    'foreColor',
    'link',
    'list',
    'justify',
    'quote',
    'image',
    'table'
  ]
}
export default editorConfig
