const vulnFormRules = { // 编辑弹窗表单验证规则
  name: [
    { required: true, message: '请输入漏洞名称', trigger: 'blur' }
  ],
  type: [
    { required: true, message: '请选择漏洞类型', trigger: 'blur' }
  ],
  level: [
    { required: true, message: '请选择漏洞等级', trigger: 'blur' }
  ],
  asset_get: [
    { required: true, message: '请选择绑定资产', trigger: 'blur' }
  ]
}
export default vulnFormRules
